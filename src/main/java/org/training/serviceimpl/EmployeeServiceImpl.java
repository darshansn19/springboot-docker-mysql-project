package org.training.serviceimpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;
import org.training.entity.Employeee;
import org.training.repository.EmployeeeRepository;
import org.training.service.EmployeeService;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class EmployeeServiceImpl implements EmployeeService {

	private EmployeeeRepository employeeeRepository;

	@Override
	public Employeee registerEmployee(Employeee employeee) {

		return employeeeRepository.save(employeee);
	}

	@Override
	public List<Employeee> getAllEmployee() {
		List<Employeee> list = new ArrayList<>();
		list = employeeeRepository.findAll();
		List<Employeee> newlist = employeeeRepository.findByNameStartingWith("d");
		System.out.println(newlist);
//		list.stream().forEach(System.out::println);

		List<Employeee> filteredEmployee = list.stream().filter(t -> t.getName().startsWith("d")).toList();

//		System.out.println(filteredEmployee);
		return list;
	}

}
