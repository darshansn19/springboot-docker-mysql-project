package org.training.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.Data;

@Entity
@Data
public class Employeee {

	@Id
	private Long id;
	private String name;
}
