package org.training.controller;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.training.entity.Employeee;
import org.training.service.EmployeeService;

import lombok.AllArgsConstructor;

@RestController
@AllArgsConstructor

public class EmployeeController {

	private EmployeeService employeeService;
	
	@PostMapping("/employees")
	public Employeee registerUser(@RequestBody Employeee employeee) {
		return employeeService.registerEmployee(employeee);
	}
	
	@GetMapping("/employee")
	public List<Employeee> getEmployee() {
		return employeeService.getAllEmployee();

	}
}
