package org.training.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.training.entity.Employeee;

@Repository
public interface EmployeeeRepository extends JpaRepository<Employeee, Long> {
 
	List<Employeee> findByNameStartingWith(String s);
}
