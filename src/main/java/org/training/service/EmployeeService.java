package org.training.service;

import java.util.List;

import org.training.entity.Employeee;

public interface EmployeeService {

	Employeee registerEmployee(Employeee employeee);

	List<Employeee> getAllEmployee();
}
